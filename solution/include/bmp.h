#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType; // 0x4D42
    uint32_t bfileSize; // Размер файла (байт)
    uint32_t bfReserved; //=0
    uint32_t bOffBits;  //смещение до поля данных (байт)
    uint32_t biSize; //размер структуры в байтах
    uint32_t biWidth; // ширина в пискелях
    uint32_t biHeight; // высота в пискелях
    uint16_t biPlanes; //=1
    uint16_t biBitCount; //количество бит на пиксель. У нас 24
    uint32_t biCompression; // BI_RGB
    uint32_t biSizeImage; // кол-во байт в поле данных
    uint32_t biXPelsPerMeter; // горизонтальное разрешение точек на дюйм
    uint32_t biYPelsPerMeter; // вертикальное разрешение точек на дюйм
    uint32_t biClrUsed; // кол-во используемых цветов
    uint32_t biClrImportant; // кол-во существенных цветов. Можно считать = 0
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAILED_PROBLEM_WITH_FILE_OR_WITH_IMAGE,
    READ_FAILED
};

enum read_status from_bmp(FILE *in, struct image *img);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FAILED_OUT_NULL,
    WRITE_FAILED_IMG_NULL
};

enum write_status to_bmp(FILE *out, const struct image *img);

struct bmp_header create_header(const struct image * img);

int get_padding(uint64_t width);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
