#ifndef ASSIGNMENT_IMAGE_ROTATION_CLOSE_H
#define ASSIGNMENT_IMAGE_ROTATION_CLOSE_H

#include <stdio.h>

enum close_status {
    CLOSE_FAILED_EOF,
    CLOSE
};

enum close_status close(FILE **file);

#endif //ASSIGNMENT_IMAGE_ROTATION_CLOSE_H
