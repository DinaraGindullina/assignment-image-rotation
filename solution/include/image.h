#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

static struct image IMAGE_NULL = (struct image){.height=0,.width=0, .data=NULL};

struct image get_image_from_file(char* file_path);

bool write_image_to_file(char* file_path, const struct image img);

struct image create_image(uint64_t height, uint64_t width);

void free_image(struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
