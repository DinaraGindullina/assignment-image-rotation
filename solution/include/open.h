#ifndef ASSIGNMENT_IMAGE_ROTATION_OPEN_H
#define ASSIGNMENT_IMAGE_ROTATION_OPEN_H

#include <stdint.h>
#include <stdio.h>

enum open_status{
    OPEN,
    OPEN_FAILED
};

enum open_status open(char *file_path,FILE **file, uint16_t modes);

#endif //ASSIGNMENT_IMAGE_ROTATION_OPEN_H
