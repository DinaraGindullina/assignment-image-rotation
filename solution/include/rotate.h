#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

struct image rotate(struct image const * rotating_img);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
