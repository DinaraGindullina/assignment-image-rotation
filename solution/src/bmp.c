#include "bmp.h"

#define bfTYPE 0x4D42
#define biSIZE 40
#define biBitCOUNT 24
#define bfRESERVED 0
#define biPLANES 1

enum read_status from_bmp(FILE *in, struct image * img) {
    struct bmp_header bh;
    if (in==NULL || img==NULL) {
        return READ_FAILED_PROBLEM_WITH_FILE_OR_WITH_IMAGE;
    }

    if(fread(&bh, sizeof(struct bmp_header), 1, in)!=1){
        return READ_FAILED;
    }


    if (bh.bfType !=bfTYPE) { return READ_INVALID_SIGNATURE; }
    if (bh.biSize!= biSIZE) { return READ_INVALID_HEADER; }
    if (bh.biBitCount != biBitCOUNT) { return READ_INVALID_BITS; }
    if (bh.bfReserved !=bfRESERVED ||
        bh.biPlanes != biPLANES){
        return READ_FAILED;
    }


    *img= create_image((uint64_t)bh.biHeight,(uint64_t)bh.biWidth);

    if(img->data==NULL){
        return READ_FAILED;
    }

    int const padding = get_padding(img->width);

    // чтение изображения
    for (int i = 0; i < img->height; i++) {
        if(fread(&(img->data[i * img->width]), sizeof (struct pixel), img->width, in)!=img->width ||
           fseek(in, padding, SEEK_CUR)!=0){
            return READ_FAILED;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    if(out==NULL){
        return WRITE_FAILED_OUT_NULL;
    }
    if(img==NULL){
        return WRITE_FAILED_IMG_NULL;
    }
    int const padding = get_padding(img->width);

    struct bmp_header bh = create_header(img);

    // запись заголовка и изображения
    if(fwrite(&bh,sizeof(struct bmp_header),1,out)!=1){
        return WRITE_ERROR;
    }
    for(size_t i = 0;i<img->height;i++){
        if(fwrite((uint8_t *) img->data+i * img->width * 3, sizeof(struct pixel), img->width, out)!=img->width ||
           fseek(out, padding, SEEK_CUR)!=0){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

int get_padding(uint64_t width){
    return (int)(width%4);
}

struct bmp_header create_header(const struct image * img){
    int const padding = get_padding(img->width);
    struct bmp_header bh = {
            .bfType = bfTYPE,
            .bfileSize = img->height * img->width * sizeof(struct pixel) +
                         img->height * padding + sizeof(struct bmp_header),
            .bfReserved = bfRESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = biSIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = biPLANES,
            .biBitCount = biBitCOUNT,
            .biCompression = 0,
            .biSizeImage =  img->height * img->width * sizeof(struct pixel) + padding*img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return bh;
}
