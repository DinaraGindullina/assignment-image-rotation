#include "close.h"

enum close_status close(FILE **file) {
    if (fclose(*file)==EOF) {
        return CLOSE_FAILED_EOF;
    }else{
        return CLOSE;
    }
}
