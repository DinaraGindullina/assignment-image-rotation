//
// Created by admin-ubuntu on 29.01.2022.
//
#include "bmp.h"
#include "close.h"
#include "open.h"
#include <stdlib.h>

struct image get_image_from_file(char* file_path){
    FILE *input = NULL;
    // открытие входного файла
    switch (open(file_path,&input,0)) {
        case OPEN_FAILED:
            fprintf(stderr,"Problem with input file");
            return IMAGE_NULL;
        case OPEN:
            break;
    }

    struct image img;

    // чтение из файла
    enum read_status read_status = from_bmp(input,&img);
    //в случае ошибки закрываем файл
    if(read_status!=READ_OK){
        close(&input);
    }
    switch (read_status) {
        case READ_FAILED_PROBLEM_WITH_FILE_OR_WITH_IMAGE:
            fprintf(stderr,"File = NULL or Image = NULL");
        case READ_INVALID_HEADER:
            fprintf(stderr,"Invalid Header");
            return IMAGE_NULL;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr,"Invalid Signature");
            return IMAGE_NULL;
        case READ_INVALID_BITS:
            fprintf(stderr,"Invalid Bits");
            return IMAGE_NULL;
        case READ_FAILED:
            fprintf(stderr,"Read error");
            return IMAGE_NULL;
        case READ_OK:
            fprintf(stderr,"Read ok");
            break;
    }

    // закрытие входного файла
    switch (close(&input)) {
        case CLOSE_FAILED_EOF:
            fprintf(stderr,"Input file is not closed");
            return IMAGE_NULL;
        case CLOSE:
            break;
    }
    return img;
}

bool write_image_to_file(char * file_path, const struct image picture){
    FILE *output = NULL;

    // открытие выходного файла
    switch (open(file_path,&output,1)) {
        case OPEN_FAILED:
            fprintf(stderr,"Problem with output file");
            return false;
        case OPEN:
            break;
    }

    // запись перевернутой картинки в файл
    enum write_status write_status = to_bmp(output,&picture);
    // в случае ошибки закрываем файл и освобождаем память
    if(write_status!=WRITE_OK){
        close(&output);
        free(picture.data);
    }
    switch (write_status) {
        case WRITE_FAILED_OUT_NULL:
            fprintf(stderr,"file = NULL");
            return false;
        case WRITE_FAILED_IMG_NULL:
            fprintf(stderr,"Image = NULL");
            return false;
        case WRITE_ERROR:
            fprintf(stderr,"Write error");
            return false;
        case WRITE_OK:
            fprintf(stderr,"Write ok");
            break;
    }
    // закрытие выходного файла
    switch (close(&output)) {
        case CLOSE_FAILED_EOF:
            fprintf(stderr,"Output file is not closed");
            return false;
        case CLOSE:
            break;
    }
    return true;
}

struct image create_image(uint64_t height, uint64_t width){
    struct image img = {
            .width=width,
            .height=height,
            .data=(struct pixel*) malloc(sizeof(struct pixel) * width * height)
    };
    return img;
}

void free_image(struct image image) {
    image.height = 0;
    image.width = 0;
    free(image.data);
}

