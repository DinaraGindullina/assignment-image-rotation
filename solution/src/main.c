#include "image.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    // проверяем кол-во аргументов
    if(argc!=3){
        fprintf(stderr,"Invalid parametrs");
        return 1;
    }

    // читаем картинку из файла
    struct image img= get_image_from_file(argv[1]);

    if(img.data==NULL){
        fprintf(stderr,"Can't read file!");
        free_image(img);
        return 1;
    }

    // поворот картинки
    struct image rotating_image = rotate(&img);

    // записываем картинку в файл
    if(!write_image_to_file(argv[2],rotating_image)){
        fprintf(stderr,"Can't write to file");
        free_image(img);
        free_image(rotating_image);
        return 1;
    }


    // освобождение памяти
    free_image(img);
    free_image(rotating_image);

    return 0;
}
