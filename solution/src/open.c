#include "open.h"
#include <stdint.h>

enum open_status open(char *file_path, FILE **file, uint16_t modes){
    if(modes==1){
        *file = fopen(file_path,"wb");
    }
    else {
        *file = fopen(file_path, "rb");
    }
    if(*file==NULL){
        return OPEN_FAILED;
    }
    return OPEN;
}
