#include "bmp.h"
#include "rotate.h"
#include <stdio.h>


struct image rotate(struct image const * rotating_img){
    struct image img = create_image(rotating_img->width,rotating_img->height);
    if(img.data==NULL){
        fprintf(stderr, "Cannot allocate memory by rotate!");
        return (struct image) {0};
    }
    for(size_t j = 0;j<img.width;j++){
        for(size_t i = 0;i<img.height;i++){
            img.data[i*img.width+j] = (rotating_img->data)[(img.width-j-1)*img.height+i];
        }
    }

    return img;
}
